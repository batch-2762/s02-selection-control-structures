# TITLE: SELECTION CONTROL STRUCTURES

# Python also allows user to input, with this, users can give inputs to the program.

# [Section] input
# f string or formatted string
# typecasting - if you want to concatenate two.

# username = input("Please enter your name: \n")
# print(f"Hello {username}! Welcome to python short course!")


# num1 = input(input("Enter first number: "))
# num2 = input(input("Enter second number: "))
# print(f"The sum of num1 and num2 is {num1+num2}")


# [Section] With user inputs, users can give inputs for the program to be used to control the application using control structures. (parang if else statement ito)
# Control structures can be divided into selection and repitition structures. (repetition: number of loops/times code is run while selection control structure, dito papasok ang selection: the condition or  selection mag stop agad after mag run ng once.)

# Selection control structures allows the program to choose among choices and run specific codes depending on the choice taken(conditions)

# Repitition Control Structures allow the program to repeat certain blocks of code given a starting condition and termination condition.


# [Section] If-else statements
# if-else statement are use to choose between two or more code blocks depending on the condition.

# test_num = 75

# if test_num >= 60:
#     print("Test passed!")
# else:
#     print("Test failed!")

# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed.

# [Section] if else chains can also be used to have more than 2 choices for the program.

# test_num2 = int(input("Please enter the second number\n"))

# if test_num2 > 0:
#     print("The number is positive!")
#     print(f"The number ")
# elif test_num2 == 0:
#     print("THe number is equal to 0!")
# else:
#     print("The number is negative!")


# Mini Exercise 1:

# Create an if-else statement that
# determines if a number is divisible by 3,
# 5, or both.

# If the number is divisible by 3, print
# "The number is divisible by 3"

# If the number is divisible by 5, print
# "The number is divisible by 5"

# If the number is divisible by 3 and 5,
# print "The number is divisible by both 3
# and 5"

# If the number is not divisible by any,
# print "The number is not divisible by 3 nor
# 5"

# test_num3 = int(input("Please enter the third number\n"))

# if test_num3 % 3 == 0:
#     print("The number is divisible by 3")

# elif test_num3 % 5 == 0:
#     print("The number is divisible by 5")

# elif test_num3 % 3 & 5 == 0:
#     print("The number is divisible by both 3 and 5")

# else:
#     print("The number is not divisible by 3 nor 5")

# int() - means to convert the number into integers.


# [Section] Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statements as long as the codition is true.
# while loop is while
# increment
# Kapag ang i += 1 ay nasa itaas ng print, mag-eend siya sa 6. Mag-increment muna siya same sa i <= 5 bago niya e printout ang i += 1 kapag nasa ibaba siya nakalagay ng i <= 5 .

# i = 0

# while i <= 5:
#     print(f"Current value of i is {i}")
#     i += 1


# [Section] for loops are used for iteraion over a sequence
# foreach() - means for in python.
# for singular in plural: sa for indiv_fruit in fruits:

fruits = ["apple", "banana", "cherry"]  # lists

for indiv_fruit in fruits:
    print(indiv_fruit)

# [Section] range() method
# To use the for loop to iterate through values, the range method can be used.
# nag iterate siya or nag start siya sa 0 at nag stop sa 5
# If d ka nag set ng start, mag ddefault siya na mag start in 0.
# if nag start tayo ng number 6, dyan tayo mag sisimula mag count ang mag sstop sa number 9 kasi nag declare tayo ng 10.
# ahh, so 6 in range(6, 10) yung starting value ni x, tapos 10 yung end ng range. Ans: YES
# iterate means to do again or to repeat again.
# ang ilalagay natin is number only sa (6) or (6, 10) or (6, 20, 2) na parang parameter. bawal sentence.

# The default starting value is 0
for x in range(6):
    print(f'The current value of x is {x}!')
# The default starting value is 6 and 10 is the stopper.
for x in range(6, 10):
    print(f'The current value of x is {x}!')

# 6 is that start, 20 will be the stopper and 2 is the incrementation.
for x in range(6, 20, 2):
    print(f'The current value of x is {x}!')


# [Section] Break Statement
# The break statement is used to stop the loop

# j = 1
# while j < 6:
#     print(j)
#     if j == 3:
#         break
#     j = +1  # increment itong j=+1 or count or loop.
# dapat may indent or tabs para ma read yung codes natin.


# [Section] Continue Statement
# The continue statement in returns the control to the beginning of the while loop and continue to the next iteration.


k = 1
while k < 6:
    k += 1

    if k == 3:
        continue
    print(k)

# It skips printing 3
