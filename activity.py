# ACTIVITY
# Accept a year input from the user and determine if it is a leap year or not.

# Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

# ACTIVITY: Strech Goal
# Add a validation for the leap year input:

# a. Strings are not allowed for inputs

# b. No zero or negative values
# Accept a year input from the user and determine if it is a leap year or not

# ANSWER:
year = int(input("Enter a year: "))

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print(year, "is a leap year")
        else:
            print(year, "is not a leap year")
    else:
        print(year, "is a leap year")
else:
    print(year, "is not a leap year")

# Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col)

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()
